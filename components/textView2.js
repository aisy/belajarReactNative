import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default class Text2 extends Component{

    render() {
        return (
            <Text>
                {this.props.title}
            </Text>
        );
    }

} 

const style= StyleSheet.create({
    textStyle:{
        color:"blue"
    }
})