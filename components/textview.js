import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

import Textview2 from './textView2.js'

export default class TextView extends Component{

    render() {
        return (
            <View>
               <Text style={style.textColor}>
                {this.props.title}
            </Text>

            <Textview2
                title={this.props.title2}
            /> 
            </View>
        );
    }


} 

const style = StyleSheet.create({
    textColor:{
        color:"red",
        fontSize: 40
    }
});