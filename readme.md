# Belajar React native bersama
Penguasaan satu minggu lebih (tergantung koneksi internet), untuk belajar awal gunakan emulator terlebih dahulu. karena kalau menggunakan device perlu mengatur device terlebih dahulu

#### Kebutuhan Development :
- Windows 10, Linux apa saja yg up to date, OSX 10.8
- RAM minimal 4GB
- Editor code apa saja(saya make vistual studio code)
- JDK versi 8
- Android SDK versi 23 (bisa di turunkan ke versi bawah), untuk emulator yang ringan bisa menggunakan Genymotion
- Xcode 9.1 (untuk yang mau develop ke ios)
- nodejs dan npm versi baru

#### Instalasi project :
1. Pastikan semua kebutuhan development sudah tersedia
2. Install react native cli (jika sudah ada lanjut ke langkah selanjutnya)
```nodejs
npm install react-native-cli
```

3. Clone atau download project 
4. Masuk ke folder project yang di clone atau di download
5. Install module kebutuhan React Native :
```nodejs
npm install
```

6. Jalankan service react native :
```nodejs
react-native start
```

7. Run/debug project ke emulator android atau ios :

```nodejs
react-native run-android
```


```nodejs
react-native run-ios
```

</br>

#### Untuk belajar bisa edit file ini :
- index.js
- App.js
- components/textView.js
- components/textView2.js

